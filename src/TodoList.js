import React, { Component } from "react";
import TodoItem from "./TodoItem";

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map(todo => (
            <TodoItem
              handleDeleteTodo={this.props.handleDeleteTodo(todo)}
              title={todo.title}
              completed={todo.completed}
              handleToggleTodo={this.props.handleToggleTodo(todo)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default TodoList;
