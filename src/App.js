import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
//import TodoItem from "./TodoItem";
import TodoList from "./TodoList";
import { Route, NavLink } from "react-router-dom";

class App extends Component {
  state = {
    todos: todosList,
    value: ""
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  handleCreateTodo = event => {
    //event.key -- represents what key the user pressed
    //use this.state.value to create a new todo
    //then, i need to update this list of todo's in component state
    if (event.key === "Enter") {
      const newTodo = {
        userId: 1,
        id: Math.floor(Math.random() * 10000),
        title: this.state.value,
        completed: false
      };
      //immutability:
      //creating copy of array (array.slice method)
      const newTodos = this.state.todos.slice();
      //modify the copy
      newTodos.push(newTodo);
      //then overwrite the original with the copy
      this.setState({ todos: newTodos, value: "" }); //include value: "" to reset input to empty
    }
  };

  handleDeleteTodo = todoIdToDelete => event => {
    //immutability:
    //create a copy
    const newTodos = this.state.todos.filter(todo => {
      if (todo.id === todoIdToDelete.id) {
        return false;
      } else {
        return true;
      }
    });
    //modify copy
    //find the index number of todoIdToDelete, once we have index number we can splice it out (remove it)

    //overwrite original with copy
    this.setState({ todos: newTodos });
  };

  handleDeleteCompletedTodos = event => {
    //.filter

    const newTodos = this.state.todos.filter(todo => {
      if (todo.completed === true) {
        return false;
      } else {
        return true;
      }
    });

    this.setState({ todos: newTodos });
  };

  handleToggleTodo = todoToggle => event => {
    //onClick, should change todo.completed state from true/false

    const newTodos = this.state.todos.map(todo => {
      if (todo.id === todoToggle.id) {
        todo.completed = !todo.completed;
      }
      return todo;
    });

    this.setState({ todos: newTodos });
  };

  countIncomplete = () => {
    return this.state.todos.filter(obj => obj.completed === false).length;
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            value={this.state.value}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo} //want to create a Todo when enter is pressed
          />
        </header>
        <Route
          path="/active"
          render={() => (
            <TodoList
              todos={this.state.todos.filter(todo => todo.completed !== true)}
              handleDeleteTodo={this.handleDeleteTodo}
              handleToggleTodo={this.handleToggleTodo}
            />
          )}
        />
        <Route
          exact
          path="/"
          render={() => (
            <TodoList
              todos={this.state.todos}
              handleDeleteTodo={this.handleDeleteTodo}
              handleToggleTodo={this.handleToggleTodo}
            />
          )}
        />
        <Route
          path="/completed"
          render={() => (
            <TodoList
              todos={this.state.todos.filter(todo => todo.completed === true)}
              handleDeleteTodo={this.handleDeleteTodo}
              handleToggleTodo={this.handleToggleTodo}
            />
          )}
        />
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>{this.countIncomplete()}</strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">
                All
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">
                Completed
              </NavLink>
            </li>
          </ul>
          <button
            onClick={this.handleDeleteCompletedTodos}
            className="clear-completed"
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

export default App;
